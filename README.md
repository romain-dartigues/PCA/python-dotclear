# README

Parse Dotclear-wiki syntax in Python.

[Dotclear](https://dotclear.org/) is a PHP written blog.

## Syntax reference

* version 1.2: http://fr.dotclear.org/documentation/1.2/usage/syntaxes
* version 2.0: http://fr.dotclear.org/documentation/2.0/usage/syntaxes

## Licence

This project is licensed under the terms of the BSD 3-clause "New" or "Revised" licence.
